# ProjetoIntermediario

O projeto intermediário consiste na implementação de uma API RESTful, integrada ao MongoDB, 
capaz de responder a solicitações de clientes através do protocolo HTTP

## Endpoints

### Characters (Personagens)

#### Descrição
Retorna informações sobre os personagens.

#### URI Padrão
> http://localhost:3000/api/characters

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único |
| name | String | Nome do personagem |
| class | String | Classe RPG do personagem |
| level | Number | Level do personagem |
| exp | Number | Total de experiência do personagem |
| health | Number | Atributo relacionado a saúde do personagem |
| armor | Number | Atributo relacionado ao valor de proteção da armadura do personagem |
| gold | Number | Descreve a quantidade de ouro (moedas) que o personagem possui |
| **attributes**
| strength | Number | Atributo relacionado a força do personagem |
| constitution | Number | Atributo relacionado a constituição do personagem |
| speed | Number | Atributo relacionado a velocidade do personagem |
| intelligence | Number | Atributo relacionado a inteligência do personagem |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/api/characters` | Retorna todos os personagens |
| GET | `api/characters/<id>` | Retorna um personagem baseado em um valor `id` único. | |
| POST | `api/characters` | Adiciona um novo personagem. |
| PUT | `api/characters/<id>` | Atualiza um personagem baseado em um valor `id` único. | |
| DELETE | `api/characters/<id>` | Remove um personagem baseado em um valor `id` único. |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=numero` | Limita a quantidade de dados de uma requisição. |
| `?class=nomeDaClasse` | Retorna somente personagens da classe definida. |

### Enemies (Inimigos)

#### Descrição
Retorna informações sobre os inimigos.

#### URI Padrão
> http://localhost:3000/api/enemies

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único. |
| name | String | Nome do inimigo. |
| class | String | Classe RPG do inimigo. |
| description | String | Descrição detalhada do inimigo. |
| level | Number | Level do inimigo. |
| health | Number | Atributo relacionado a saúde do inimigo. |
| armor | Number | Atributo relacionado ao valor de proteção da armadura do inimigo. |
| **attributes**
| strength | Number | Atributo relacionado a força do inimigo. |
| constitution | Number | Atributo relacionado a constituição do inimigo. |
| speed | Number | Atributo relacionado a velocidade do inimigo. |
| intelligence | Number | Atributo relacionado a inteligência do inimigo. |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/api/enemies` | Retorna todos os inimigos. |
| GET | `api/enemies/<id>` | Retorna um inimigo baseado em um valor `id` único. | |
| POST | `api/enemies` | Adiciona um novo inimigo. |
| PUT | `api/enemies/<id>` | Atualiza um inimigo baseado em um valor `id` único. | |
| DELETE | `api/enemies/<id>` | Remove um inimigo baseado em um valor `id` único. |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=numero` | Limita a quantidade de dados de uma requisição. |
| `?level=numero` | Retorna somente inimigos do level definido. |

### Items (Itens)

#### Descrição
Retorna informações sobre os itens.

#### URI Padrão
> http://localhost:3000/api/items

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único. |
| name | String | Nome do item. |
| description | String | Descrição detalhada do item. |
| effect | String | Efeito relacionado ao uso do item. |
| owner | Schema.Types.ObjectId | Valor `id` do personagem (character) dono do item. |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/api/items` | Retorna todos os itens. |
| GET | `api/items/<id>` | Retorna um item baseado em um valor `id` único. | |
| POST | `api/items` | Adiciona um novo item. |
| PUT | `api/items/<id>` | Atualiza um item baseado em um valor `id` único. | |
| DELETE | `api/items/<id>` | Remove um item baseado em um valor `id` único. |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=numero` | Limita a quantidade de dados de uma requisição. |
| `?effect=nomeDoEfeito` | Retorna somente os itens com o efeito definido. |

### Maps (Mapas)

#### Descrição
Retorna informações sobre os mapas.

#### URI Padrão
> http://localhost:3000/api/maps

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único. |
| name | String | Nome do mapa. |
| description | String | Descrição detalhada do mapa. |
| type | String | Tipo de terreno do mapa (Jungle, Mountain, Castle) 

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/api/maps` | Retorna todos os mapas. |
| GET | `api/maps/<id>` | Retorna um mapa baseado em um valor `id` único. | |
| POST | `api/maps` | Adiciona um novo mapa. |
| PUT | `api/maps/<id>` | Atualiza um mapa baseado em um valor `id` único. | |
| DELETE | `api/maps/<id>` | Remove um mapa baseado em um valor `id` único. |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=numero` | Limita a quantidade de dados de uma requisição. |
| `?type=tipoDoMapa` | Retorna somente mapas com o tipo definido. |

### Places (Locais)

#### Descrição
Retorna informações sobre os locais de um mapa.

#### URI Padrão
> http://localhost:3000/api/places

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único. |
| name | String | Nome do local. |
| description | String | Descrição detalhada do local. |
| map | Schema.Types.ObjectId | Valor `id` do mapa (map) que contém este local (place). |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/api/places` | Retorna todos os locais. |
| GET | `api/places/<id>` | Retorna um local baseado em um valor `id` único. | |
| POST | `api/places` | Adiciona um novo local. |
| PUT | `api/places/<id>` | Atualiza um local baseado em um valor `id` único. | |
| DELETE | `api/places/<id>` | Remove um local baseado em um valor `id` único. |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=numero` | Limita a quantidade de dados de uma requisição. |
| `?map=idMap` | Retorna somente os locais de um mesmo mapa com o `id` definido. |

### Users (Usuários)

#### Descrição
Retorna informações sobre os usuários.

#### URI Padrão
> http://localhost:3000/api/users

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único. |
| name | String | Nome do usuário. |
| password | String | Senha de acesso. |
| admin | Boolean | Referente ao tipo de usuário (administrador ou não). |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/api/users` | Retorna todos os usuários. |
| GET | `api/users/<id>` | Retorna um usuário baseado em um valor `id` único. | |
| POST | `api/users` | Adiciona um novo usuário. |
| PUT | `api/users/<id>` | Atualiza um usuário baseado em um valor `id` único. | |
| DELETE | `api/users/<id>` | Remove um usuário baseado em um valor `id` único. |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=numero` | Limita a quantidade de dados de uma requisição. |
| `?admin=boolean` | Retorna usuários administradores ou não, de acordo com filtro definido (true ou false) |

## Autenticação

O processo de autenticação se da por meio do arquivo **authentication.js**.
Busca-se um usuário cadastrado no banco de dados por meio de um query, por padrão definida como: 

`var` query = { **admin**: true, **name**: 'Bernardo', **password**: 'adm123' }

Caso não exista um usuário cadastrado com os dados informados, a conexão com o banco é fechada, não permitindo a manipulação e acesso as rotas, do contrário o acesso é liberado.