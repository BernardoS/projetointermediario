const mongoose = require('mongoose')
mongoose.set('useFindAndModify', false);


// Conexão com o MongoDB
var url = "mongodb://localhost/projeto"
var options = {
    useUnifiedTopology: true,
    useNewUrlParser: true
}

mongoose.connect(url, options)

// Resposta da Conexão
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("O mongo foi conectado...")
});

module.exports = mongoose