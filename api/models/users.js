const mongoDB = require('../data/connection')
var Schema = mongoDB.Schema

var userSchema = new Schema({
    name: { type: String, required: true },
    password: { type: String, required: true },
    admin: { type: Boolean, default: false }
});

var user = mongoDB.model('User', userSchema)

module.exports = user