const mongoDB = require('../data/connection')
var Schema = mongoDB.Schema

var placeSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    map: {
        type: Schema.Types.ObjectId,
        ref: 'Map',
        default: null
    }
});

var place = mongoDB.model('Place', placeSchema)

module.exports = place