const mongoDB = require('../data/connection')
var Schema = mongoDB.Schema

var itemSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    effect: { type: String, required: true, default: "Sem efeito" },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'Character',
        default: null
    }
});

var item = mongoDB.model('Item', itemSchema)

module.exports = item